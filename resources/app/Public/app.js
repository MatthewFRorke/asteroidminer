// Game
var config = {
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: window.innerHeight,
    backgroundColor: '#fff',
    pixelArt: true,
    scale: {
        mode: Phaser.Scale.RESIZE,
        parent: 'phaser-example',
        width: '100%',
        height: '100%'
    },
    physics: {
        default: 'arcade',
        arcade: {
            useTree: false,
            gravity: { y: 300 },
            debug: window.remoteDebug
        }
    },
    dom: {
        createContainer: true
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    },
};

var userConfig = {
    myPlayer: undefined,
}

var playerObj;

var socket;
var terrain = {};
var playerList = {};

var bg = undefined;

var phaser, world;

function startLoadingGame() {
    phaser = new Phaser.Game(config);
    world = new World(phaser);
    window.setLoading("Creating Game");
}

var deadList = [];
var deadListLock = false;

var sound;

function preload() {
    world.loadImages(this);
    world.loadSounds(this);
    window.setLoading("Loading Sprites");
}

function create() {
    world.loadAnimations(this);
    onGameLoad();
    window.setLoading("Loading Animations");
    createServer(this);
}

function createServer(phaserCtx) {
    network = new Network();
    
    window.setLoading("Establishing Network Connection");    
    network.onCreateWorld = function(worldData) {
        window.setLoading("Generating Map");
        world.createBackground(phaserCtx);
        world.loadMap(phaserCtx, worldData);
        phaserCtx.physics.world.setBounds(0, 0, worldData.width, worldData.height, 1);
        window.setLoading("Reticulating Splines");
        document.getElementById("loading").style.display="none";
        document.getElementById("join").style.display="block";
    };

    network.onCreatePlayer = function(socketId, serverSidePlayerObject) {
        playerList[socketId] = {};
        playerList[socketId] = new Player(socketId, phaserCtx, serverSidePlayerObject.name, serverSidePlayerObject.position.x, serverSidePlayerObject.position.y, [world.terrainGroup]);
    };

    network.onCreateLocalPlayer = function(socketId, serverSidePlayerObject) {
        userConfig.myPlayer = socketId;
        playerList[socketId] = new Player(socketId, phaserCtx, serverSidePlayerObject.name, serverSidePlayerObject.position.x, serverSidePlayerObject.position.y, [world.terrainGroup]).setAsMe(1024, 768);
        playerList[socketId].onPlayerStartFlight = function() {
            sound.doFlight();
        };
        playerList[socketId].onPlayerStopFlight = function() {
            sound.doStopFlight();
        };
        
        playerList[socketId].myContext.onDestroyTerrain = function(terrainId) {
            terrain[terrainId].destroy(playerList[userConfig.myPlayer]);
        };
        
        setInterval(function() {
                UpdateCoords(network, playerList[userConfig.myPlayer]);
            },
            100
        );
    };

    network.onPlayerRemove = function(socketId) {
        if(!playerList[socketId]) {
            return;
        }
        playerList[socketId].remove(); // Verify slot doesn't already exist before just unsetting
        delete playerList[socketId];
    };

    network.onPlayerPositionUpdate = function(socketId, gameStatus) {
        if(!playerList[socketId]) {
            return;
        }
        var player = playerList[socketId];
        if(player.isMe) {
            return;
        }
        player.setPosition(gameStatus[socketId].position.x, gameStatus[socketId].position.y, gameStatus[socketId].angle, false);
        player.isFlying = gameStatus[socketId].isFlying;
        if(player.isFlying) {
            player.doFlight();
        } else {
            player.doStopFlight();
        }
        player.setStaticAnimation(gameStatus[socketId].currentAnim, player.gameObject.setFlippedX);
        player.gameObject.setFlipX(gameStatus[socketId].flipX);
    };

    network.onRemoveTerrain = function(id) {
        terrain[id].setAsDestroyed();
        sound.doDigDestroy();
    };
    
    network.onAddItemToInventory = function(itemId) {
        var player = playerList[userConfig.myPlayer];
        player.setInventory(world.worldItemRef[itemId]);
    };

    console.log('creating', world);
    world.onTerrainRequestDestroy = function(tId, playerId) {
        network.destroyRequestBulk([{terrainId: tId, playerId: playerId}]);
    } 
    console.log('network created');
   
    sound = new Sound(phaser);
    
}

function update() {
    sound.checkAmbience();
    if(!playerList[userConfig.myPlayer]) {
        return;
    }
    var player = playerList[userConfig.myPlayer];

    player.updateTracker();
    if (player.isMovingLeft) {
        player.doMoveLeft();
        if(player.gameObject.anims.currentAnim.key == "playerRun") {
            sound.doFootstep();
        }
    }
    if (player.isMovingRight) {
        player.doMoveRight();
        if(player.gameObject.anims.currentAnim.key == "playerRun") {
            sound.doFootstep();
        }
    }
    if (player.isMovingUp ) {
        player.doMoveUp();
    }
    if(player.isFlying) {
        player.doFlight();
    }
    if(player.gameObject.y >= 10*64) {
        sound.setIsBelowWorld();
    } else if(sound.isBelowWorld) {
        sound.setIsNotBelowWorld();
    }
}

function onGameLoad() {
    document.getElementById("join").addEventListener("click", function() {
        document.getElementById("join").style.display="none";
        network.userRequestSpawn("GUEST");
    });
    
    setInterval(function() {
            if(deadList.length > 0 && !deadListLock) {
                deadListLock = true;
                network.destroyRequestBulk(deadList);
                deadList = [];
                deadListLock = false;
            }
        },
        1000
    );
}

function UpdateCoords(network, player) {
    // console.log("updating coords");
    network.playerMoveUpdate(player);
}