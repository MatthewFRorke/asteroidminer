class Player {
    isSpawned = false;
    isMovingLeft = false;
    isMovingRight = false;
    isMovingUp = false;
    isMovingDown = false;
    isMe = false;
    isFlying = false;
    nearbyTerrain = {};
    phaser;
    gameObject;
    nameTag;
    nameTagOffset = {x: -16, y:-45};
    nearbyTerrainKeys = [];
    cameraFollow = false;
    tweens;

    MOVE_SPEED = 300;

    constructor(GameContext, name, spawnX, spawnY) {
        this.phaser = GameContext;
        this.gameObject = this.createGameObject(spawnX, spawnY);
        this.spawn(spawnX,spawnY, true);
        this.nameTag = this.phaser.add.text(0, 0, 'Guest', { font: "12px Helvetica", fill: "#ffbb00" })
        this.nameTag.setStroke('#000', 4);
        this.tweens = [];
    }

    setAsMe(mapWidth, mapHeight) {
        var classCtx = this;
        this.phaser.input.keyboard.on('keydown', function (e) {
            switch (e.keyCode) {
                case 65: // a
                    classCtx.doRunAnimation();
                    classCtx.isMovingLeft = true;
                    classCtx.isMovingRight = false;
                    break;
                case 68: // d
                    classCtx.doRunAnimation(false);
                    classCtx.isMovingRight = true;
                    classCtx.isMovingLeft = false;
                    break;
                case 87: // w
                    classCtx.isMovingUp = true;
                    break;
                case 83: // s
                    classCtx.isMovingDown = true;
                    break;
                case 32: // space
                    if(classCtx.isMovingUp) {
                        classCtx.isMovingUp = true;
                        classCtx.isFlying = true;
                    }
                    classCtx.isMovingUp = true;
                    break;
            }
        });
        this.phaser.input.keyboard.on('keyup', function (e) {
            switch (e.keyCode) {
                case 65: // a
                    classCtx.isMovingLeft = false;
                    classCtx.doStopMoveLeftOrRight();
                    break;
                case 68: // d
                    classCtx.isMovingRight = false;
                    classCtx.doStopMoveLeftOrRight();
                    break;
                case 83: // s
                    classCtx.isMovingDown = false;
                    break;
                case 87: // w
                    classCtx.isMovingUp = false;
                    break;
                case 32: // space
                    classCtx.doStopFlight();
                    classCtx.isFlying = false;
                    break;
            }
            if(!classCtx.isMovingRight && !classCtx.isMovingLeft && classCtx.gameObject.anims.currentAnim.key != 'playerIdle') {
                classCtx.doIdle();
            }
        });
        this.isMe = true;
        this.phaser.cameras.main.startFollow(this.gameObject, true);
        this.cameraFollow = true;
        this.phaser.cameras.main.setBounds(0, 0);
        this.isFlying = true;
        this.gameObject.angle = 180;
        this.jetBoots();
        return this;
    }

    haveHitboxesLoaded() {
        return (this.nameTag);
    }

    updateTracker() {
        if(!this.gameObject || !this.haveHitboxesLoaded()) {
            return;
        }
        var gameObjectPositionFeet = this.gameObject.getCenter();
        if(this.isFlying) {
            if(this.animation.emitter) {
                this.animation.emitter.setPosition(gameObjectPositionFeet.x-16, gameObjectPositionFeet.y)
            }
        }
        //this.trackerFeet.setPosition(gameObjectPositionFeet.x, gameObjectPositionFeet.y+28);
       //this.trackerRightArm.setPosition(gameObjectPositionFeet.x+16, gameObjectPositionFeet.y);
        //this.trackerLeftArm.setPosition(gameObjectPositionFeet.x-16, gameObjectPositionFeet.y);

        this.nameTag.setPosition(gameObjectPositionFeet.x+this.nameTagOffset.x, gameObjectPositionFeet.y+this.nameTagOffset.y);
        // this.trackerLeftArm.setPosition(gameObjectPositionFeet.x-16, gameObjectPositionFeet.y);
        // this.trackerRightArm.setPosition(gameObjectPositionFeet.x+16, gameObjectPositionFeet.y);
    }

    setNearbyTerrain(key, terrainItem) {
        if(this.nearbyTerrainKeys.indexOf(key) !== -1) {
            return;
        }
        this.nearbyTerrain[key] = terrainItem;
        this.nearbyTerrainKeys.push(String(key));
        return this;
    }

    unsetNearbyTerrain(terrainId) {
        var terrainIndex = this.nearbyTerrainKeys.indexOf(String(terrainId));
        if(terrainIndex != -1) {
            delete this.nearbyTerrain[terrainId];
            this.nearbyTerrainKeys.splice(terrainIndex, 1);
        }
    }

    pointCheck(x, y, x2, y2, range) {
        var theX = x2 - x;
        var theY = y2 - y;
        var rangeCheck = Math.sqrt((theX * theX) + (theY * theY))/10;
        return (rangeCheck < (range));
    }

    setPosition(x, y, angle = 0, teleport = true) {
        if(!this.gameObject) {
            return;
        }
        if(!teleport) {
            if(this.tweens[0]) {
                this.tweens[0].stop();
            }
            this.tweens[0] = this.phaser.tweens.add({
                targets: this.gameObject,
                x: x,
                y: y,
                duration: 100,
                ease: 'Power2'
            });
            this.gameObject.angle = angle;
            if(this.tweens[1]) {
                this.tweens[1].stop();
            }
            this.tweens[1] = this.phaser.tweens.add({
                targets: this.nameTag,
                x: x+this.nameTagOffset.x,
                y: y+this.nameTagOffset.y,
                duration: 100,
                ease: 'Power2'
            });
        } else {
            this.gameObject.x = x;
            this.gameObject.y = y;
        }
    }

    setScale(scale) {
        this.gameObject.scaleX = scale;
    }

    spawn(x, y, noPositionChange = false) {
        if(!this.gameObject) {
            throw error("No game object found");
        }
        if(!noPositionChange) {
            this.gameObject.x = x;
            this.gameObject.y = y;
        }
        this.gameObject.play('playerIdle');
    }

    remove() {
        if(this.tweens[0]) {
            this.tweens[0].stop();
        }
        if(this.tweens[1]) {
            this.tweens[1].stop();
        }
        this.gameObject.setActive(false);
        this.gameObject.destroy();
        this.nameTag.destroy();
    }

    createGameObject(x = 0, y = 0) {
        var player = this.phaser.physics.add.sprite(x,y,'player').setCollideWorldBounds();
        player.debugShowVelocity = true;
        return player;
    }

    getPosition() {
        if(!this.gameObject) {
            throw error("No game object found");
        }
        return { x: this.gameObject.x, y: this.gameObject.y };
    }
    animation = { particles: undefined, emitter: undefined, isEmitting: undefined };

    jetBoots() {
        if(!this.animation.particles) {
            this.animation.particles = this.phaser.add.particles('muzzle');
            if(!this.animation.emitter) {
                this.animation.emitter = this.animation.particles.createEmitter({
                    speed: 100,
                    x:0,
                    y:0,
                    scale: { start: 0.1, end: 0 },
                    blendMode: 'ADD'
                }).setVisible(false);
            }
            console.log("animation particles", this.animation);
        }
    }

    doRunAnimation(goLeft = true) {
        if(this.isFlying) {
            return;
        }
        if(goLeft) {       
            this.gameObject.setFlipX(true);
        } else {
            this.gameObject.setFlipX(false);
        }
        if((this.gameObject.anims.currentAnim && this.gameObject.anims.currentAnim.key != 'playerRun') || (!this.gameObject.anims.currentAnim)) {
            this.gameObject.play('playerRun');
        }
    }

    setStaticAnimation(anim) {
        if(!this.gameObject.anims.currentAnim || (this.gameObject.anims.currentAnim.key == anim)) {
            return;
        }
        this.gameObject.play(anim);
    }

    doMoveLeft() {
        if(this.isFlying) {
            this.gameObject.setAngularVelocity(-180);
            return;
        }
        this.gameObject.setVelocityX(-180);
    }

    doMoveRight() {
        if(this.isFlying) {
            this.gameObject.setAngularVelocity(180);
            return;
        }
        this.gameObject.setVelocityX(180);
    }
    
    doStopMoveLeftOrRight() { 
        this.gameObject.setVelocity(0);
        this.gameObject.setAngularVelocity(0);
    }

    doMoveUp() {
        if(this.gameObject.body.touching.down && !this.isFlying) {
            this.gameObject.setVelocityY(-300);
        }
    }

    doFlight() {
        if(!this.animation.emitter.visible) {
            this.animation.emitter.setVisible(true);
        }
        var angle = Math.round(this.gameObject.angle+180);

        var x=0;
        var y=0;
        var yvelocity = -1;
        var xvelocity = 0;
        
        if(angle >= 0 && angle < 90) {
            x=Math.round(angle*1,11);
            y=100-x;
            yvelocity = y; // top
            xvelocity = x * (-1); // right
        } else if(angle >= 90 && angle < 180) {
            y=Math.round((angle-90)*1,11);
            x=100-y;
            yvelocity = y * (-1); // bottom
            xvelocity = x * (-1); // right
        } else if(angle >= 180 && angle <270) {
            x=Math.round((angle-180)*1,11);
            y=100-x;
            yvelocity = y * (-1); // bottom
            xvelocity = x; // left
        } else if(angle >= 270 && angle <= 360) {
            y=Math.round((angle-270)*1,11);
            x=100-y;
            yvelocity = y; // top
            xvelocity = x; // left
        }

        var speed = 4;
        xvelocity = xvelocity*speed;
        yvelocity = yvelocity*speed;
        this.gameObject.setVelocity(xvelocity, yvelocity);
        //this.gameObject.setGravityY(300);
        this.gameObject.play('playerIdle');
    }
    
    doStopFlight() {
        console.log('stop flying', this.gameObject);
        if(this.animation.emitter) {
            this.animation.emitter.setVisible(false);
        }
    }

    doIdle() {
        this.gameObject.play('playerIdle');
    }
}