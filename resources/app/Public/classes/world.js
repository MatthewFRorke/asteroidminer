class World {
    worldData = {};
    worldItemRef = {};
    terrainGroup;
    onTerrainRequestDestroy = function() { };
    bg;
    bgWidth = 2048;
    bgHeight = 1080;
    phaser;

    constructor(ctx) {
        this.phaser = ctx;
    }

    loadImages(ctx) {
        // Ground Textures
        ctx.load.image('ground', './assets/Ground/ground.png');
        ctx.load.image('ground-2', './assets/Ground/ground-2.png');
        ctx.load.image('copper', './assets/Ground/copper.png');
        ctx.load.image('iron', './assets/Ground/iron.png');
        ctx.load.image('feldspar', './assets/Ground/feldspar.png');
        
        // Player Atlas
        ctx.load.atlas('player', './assets/Engineer/engineer-full.png', './assets/Engineer/engineer-idle.json'); 
        
        // Particles
        ctx.load.image('muzzle', 'assets/Mining/muzzleflash.png');
        
        // Cloud
        ctx.load.image('cloud', 'assets/Cloud/cloud.png')
    }

    loadSounds(gameCtx) {
        gameCtx.load.audio('ambience', 'sounds/ambience/ambience.mp3');
        gameCtx.load.audio('footstep-1', 'sounds/player/footstep-1.mp3');
        gameCtx.load.audio('footstep-2', 'sounds/player/footstep-2.mp3');
        gameCtx.load.audio('dig-destroy', 'sounds/player/dig-destroy.mp3');
        gameCtx.load.audio('flight', 'sounds/player/flight.mp3');
    }

    loadAnimations(gameCtx) {
        gameCtx.anims.create(
            { 
                key: 'playerIdle',
                frames: gameCtx.anims.generateFrameNames('player',
                    { start: 0, end: 7, prefix: 'idle' },
                ), frameRate: 12, repeat: -1 
            }
        );
        gameCtx.anims.create(
            { 
                key: 'playerRun',
                frames: gameCtx.anims.generateFrameNames('player',
                    { start: 0, end: 7, prefix: 'run' },
                ), frameRate: 12, repeat: -1 
            }
        );
        gameCtx.anims.create(
            { 
                key: 'playerMineVert',
                frames: gameCtx.anims.generateFrameNames('player',
                    { start: 0, end: 0, prefix: 'mine-vert' },
                ), frameRate: 0, repeat: -1 
            }
        );
        gameCtx.anims.create(
            { 
                key: 'playerMineHor',
                frames: gameCtx.anims.generateFrameNames('player',
                    { start: 0, end: 0, prefix: 'mine-hor' },
                ), frameRate: 0, repeat: -1 
            }
        );
    }

    destroyTerrainRequest(terrainId, playerId) {
        this.onTerrainRequestDestroy(terrainId, playerId);
    }

    createBackground(phaser) {
        this.bg = phaser.add.graphics();
        this.bg.fillGradientStyle(0x000000, 0x000000, 0x021F4B,  0x021F4B, 1);
        this.bg.fillRect(0,0,this.bgWidth,this.bgHeight);
        this.bg.setScrollFactor(0, 1);  
    }

    loadMap(gameCtx, world) {
        console.log('loading map');
        var terrainGroup;
        var worldData = world.map;
        var worldMapKeys = Object.keys(worldData);
        var worldItemRef = world.ref;
        var terrainGameObjects = [];
        var ctx = this;
        var debug = {
            copper: 0,
            total: 0
        }
        worldMapKeys.forEach(function(rowKey, rkey) {
            rkey = rkey+world.plus; // offset for above ground
            var row = worldData[rowKey];
            if(!row.exists) {       
                new Terrain(gameCtx, worldItemRef[row.type], row.x, row.y, true);
                return;
            }
            if(row.type == 2) {
                debug.copper++;
            }
            debug.total++;
            var newTerrain = new Terrain(gameCtx, worldItemRef[row.type], row.x, row.y);
            newTerrain.onRequestTerrainDestroyed = function(terrainId, playerId) { ctx.destroyTerrainRequest(terrainId, playerId) };
            newTerrain.gameObject.name = rowKey;
            terrainGameObjects.push(newTerrain.gameObject);
            terrain[rowKey] = newTerrain;
        });
        terrainGroup = gameCtx.physics.add.staticGroup(terrainGameObjects, {
            immovable: true
        });
        terrainGroup.refresh();  
        console.log('generated map');  
        this.terrainGroup = terrainGroup;
        this.worldItemRef = worldItemRef;
        return terrainGroup;
    }
}