class Terrain {
    phaser;
    gameObject;
    tileWidth = 64;
    tileHeight = 64;
    health = 60;
    animation = {
        emitter: undefined,
        particles: undefined,
        isActive: false
    };
    x;
    y;
    onRequestTerrainDestroyed = function() {};

    constructor(ctx, const_type, spawnX, spawnY, backgroundOnly = false) {
        this.phaser = ctx;
        ctx.physics.add.staticImage(spawnX+32, spawnY, 'ground-2').setTint(0x222222);
        if(!backgroundOnly) {
            this.gameObject = ctx.physics.add.staticImage(spawnX+32, spawnY, const_type);
        }
        this.x = spawnX;
        this.y = spawnY;
        //this.gameObject.setFrictionX(0);
        //this.gameObject.setFriction(0, 1);
    }

    getPosition() {    
        if(!this.gameObject) {
            return;
        }
        return {
            x: this.x,
            y: this.y
        };
    }

    destroy(destroyedBy = undefined, full = false) { // The name of this method is legacy. It actually requests damage to be dealt
        this.onRequestTerrainDestroyed(this.gameObject.name, destroyedBy.gameObject.name);
        return false;
    }

    setAsDestroyed() { // this is the method that actually destroys
        this.gameObject.destroy();
        this.gameObject = undefined;
    }
}