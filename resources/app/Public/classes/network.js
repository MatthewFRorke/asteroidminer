class Network {
    socket = undefined;
    onCreateWorld = function() { };
    onCreatePlayer = function() { };
    onCreateLocalPlayer = function() { };
    onPlayerRemove = function() { };
    onPlayerPositionUpdate = function() { };
    onRemoveTerrain = function() { };
    onAddItemToInventory = function() { };
    
    constructor() {
        this.socket = io.connect(window.remoteHost);
        this.init(this.socket);
    }

    destroyRequestBulk(ids) {
        this.socket.emit('destroyTerrain', {terrainIds: ids});
    }

    userRequestSpawn(name = "GUEST") {
        this.socket.emit('playerJoin', { name: name })
    }

    playerMoveUpdate(player) {
        var currAnim = 'playerIdle';
        if(player.gameObject.anims.currentAnim && player.gameObject.anims.currentAnim.key) {
            currAnim = player.gameObject.anims.currentAnim.key;
        }
        var flipped = player.gameObject.flipX;
        this.socket.emit("playerMoveUpdate", {
            position: {x: player.gameObject.x, y: player.gameObject.y},
            anim: currAnim,
            flipX: flipped,
            isFlying: player.isFlying
        });
    }

    init(socket) {
        var ctx = this;
        socket.on("gameStatus", function(incomingObj) {
            ctx.onCreateWorld(incomingObj.world);
            var playerSocketIds = Object.keys(incomingObj.players);
            if(playerSocketIds.length > 0) {
                playerSocketIds.forEach(socketId => {
                    ctx.onCreatePlayer(socketId, incomingObj.players[socketId]);
                });
            }
        });

        socket.on("addUser", function(incomingObj) {
            var {
                socketId,
                player
            } = incomingObj;
            ctx.onCreatePlayer(socketId, player);
            // gameCtx.physics.add.collider(playerList[socketId].gameObject, terrainGroup);
        });

        socket.on("removeUser", function(incomingObj) {
            var {
                socketId
            } = incomingObj;
            ctx.onPlayerRemove(socketId);
        });
  
    
        socket.on("removeTerrain", function(incomingArr) {
            incomingArr.terrainIds.forEach(function(id) {
                ctx.onRemoveTerrain(id);
            });
        });    
        
        socket.on("spawnSuccess", function(incomingObj) {
            ctx.onCreateLocalPlayer(incomingObj.socketId, incomingObj.player);
        });
    
        socket.on("positionUpdate", function(gameStatus) {
            Object.keys(gameStatus).forEach(function(socketId) {
                ctx.onPlayerPositionUpdate(socketId, gameStatus);
            });
        });
    
        socket.on("addItemToInventory", function(incomingObj) {
            for(var i = 0; i < incomingObj.count; i++) {
                ctx.onAddItemToInventory(incomingObj.itemId);
            }
        });
    }
}