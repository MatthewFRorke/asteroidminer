class Player {
    isSpawned = false;
    isMovingLeft = false;
    isMovingRight = false;
    isMovingUp = false;
    isMovingDown = false;
    isMe = false;
    isFlying = false;
    
    inventory = {};
    inventoryText = undefined;

    collider = undefined;
    collidingGroups = [];

    nameTag = 'Guest';
    myContext = undefined;
    phaser = undefined;
    gameObject = undefined;
    
    animation = {
        emitter: undefined,
        particles: undefined
    }

    tween = {
        movePlayer : undefined,
        movePlayerName : undefined
    }

    nameTagOffset = {
        x: -16,
        y: -44
    }

    onPlayerStartFlight = function() {};
    onPlayerStopFlight = function() {};

    constructor(socketId, gameContext, name, spawnX, spawnY, collidingGroups = []) {
        this.phaser = gameContext;

        this.gameObject = this.phaser.physics.add.sprite(spawnX,spawnY,'player').setCollideWorldBounds();

        this.gameObject.name = socketId;
        this.gameObject.body.moves = false;
        this.gameObject.debugShowVelocity = true;
        this.inventoryText = this.phaser.add.text(0, 0, '').setScrollFactor(0);
        this.collidingGroups = collidingGroups;
        if(collidingGroups.length > 0) {
            // add collisions for all specified groups
            var gameCtx = this.gameObject;
            var ctx = this;
            collidingGroups.forEach(function(group) {
                ctx.collider = gameContext.physics.add.collider(gameCtx, group);
            });
        }

        this.nameTag = this.phaser.add.text(0, 0, 'Guest', { font: "12px Helvetica", fill: "#ffbb00" })
        this.nameTag.setStroke('#000', 4);
        this.tweens = [];

        this.spawn(spawnX,spawnY, true);

        if(!this.animation.particles) {
            this.animation.particles = this.phaser.add.particles('muzzle');
            if(!this.animation.emitter) {
                this.animation.emitter = this.animation.particles.createEmitter({
                    speed: 50,
                    quantity: 5,
                    scale: { start: 0.20, end: 0 },
                    blendMode: 'ADD'
                }).startFollow(this.gameObject, 0, 22);
                this.animation.emitter.on = false;
            }
        }
    }

    updateTracker() {
        if(!this.gameObject) {
            return;
        }
        if(this.myContext) {
            var playerCenter = this.gameObject.getCenter();
            var playerBottomCenter = this.gameObject.getBottomCenter();
            if(this.myContext.hitboxes.bottom) {
                this.myContext.hitboxes.bottom.setPosition(playerBottomCenter.x, playerBottomCenter.y+16);
            }
            if(this.myContext.hitboxes.left) {
                this.myContext.hitboxes.left.setPosition(playerCenter.x-16, playerCenter.y);
            }
            if(this.myContext.hitboxes.right) {
                this.myContext.hitboxes.right.setPosition(playerCenter.x+16, playerCenter.y);
            }
        }
        var gameObjectPositionFeet = this.gameObject.getCenter();
        // name tag follow
        this.nameTag.setPosition(gameObjectPositionFeet.x+this.nameTagOffset.x, gameObjectPositionFeet.y+this.nameTagOffset.y);
    }

    spawn(x, y, noPositionChange = false) {
        if(!this.gameObject) {
            throw error("No game object found");
        }
        if(!noPositionChange) {
            this.gameObject.x = x;
            this.gameObject.y = y;
        }
        this.gameObject.play('playerIdle');
    }

    setAsMe() {
        this.isMe = true;
        this.gameObject.body.moves = true;
        this.myContext = new ControllablePlayer(this);
        return this;
    }

    myCollide() {
        if(!this.isMe) {
            return;
        }
        var myCtx = this.myContext;
        this.collider.collideCallback = function(a,b) {
            myCtx.onCollide(a,b); // QUIRK order in arguments on purpose for a retrofit
        };
    }

    setInventory(item, amount = 1) {
        if(!this.inventory[item]) {
            this.inventory[item] = 0;
        }
        this.inventory[item] += amount;
        this.updateInventoryText();
    }

    updateInventoryText() {
        var inventoryKeys = Object.keys(this.inventory);
        var text = '';
        var ctx = this;
        inventoryKeys.forEach(function(key) {
            text += ctx.inventory[key] + "x " + key + "\n";
        });
        this.inventoryText.setText(text);
    }

    setPosition(x, y, angle = 0, teleport = true) {
        if(!this.gameObject) {
            return;
        }
        if(!teleport) {
            if(this.tweens.movePlayer) {
                this.tweens.movePlayer.stop();
            }
            this.tweens.movePlayer = this.phaser.tweens.add({
                targets: this.gameObject,
                x: x,
                y: y,
                duration: 100,
                ease: 'Power2'
            });
            this.gameObject.angle = angle;
            if(this.tweens.movePlayerName) {
                this.tweens.movePlayerName.stop();
            }
            this.tweens.movePlayerName = this.phaser.tweens.add({
                targets: this.nameTag,
                x: x+this.nameTagOffset.x,
                y: y+this.nameTagOffset.y,
                duration: 100,
                ease: 'Power2'
            });
        } else {
            this.gameObject.x = x;
            this.gameObject.y = y;
        }    
    }

    setStaticAnimation(anim) {
        if(!this.gameObject.anims.currentAnim || (this.gameObject.anims.currentAnim.key == anim)) {
            return;
        }
        this.gameObject.play(anim);
    }

    doRunAnimation(goLeft = true) {
        if(goLeft) {       
            this.gameObject.setFlipX(true);
        } else {
            this.gameObject.setFlipX(false);
        }
        if((this.gameObject.anims.currentAnim && this.gameObject.body.touching.down && this.gameObject.anims.currentAnim.key != 'playerRun') || (!this.gameObject.anims.currentAnim)) {
            this.gameObject.play('playerRun');
        }
    }

    doMoveLeft() {
        this.gameObject.setVelocityX(-180);
        this.doRunAnimation();
    }

    doMoveRight() {
        this.gameObject.setVelocityX(180);
        this.doRunAnimation(false);
    }
    
    doStopMoveLeftOrRight() { 
        this.gameObject.setVelocityX(0);
        this.gameObject.play('playerIdle');
    }

    doMoveUp() {
        if(this.gameObject.body.touching.down && !this.isFlying) {
            this.gameObject.setVelocityY(-220);
        }
    }

    remove() {
        if(this.tweens.movePlayer) {
            this.tweens.movePlayer.stop();
        }
        if(this.tweens.movePlayerName) {
            this.tweens.movePlayerName.stop();
        }
        this.gameObject.setActive(false);
        this.gameObject.destroy();
        this.nameTag.destroy();
    }

    doFlight() {
        if(this.gameObject.anims.currentAnim.key != 'playerIdle') {
            this.gameObject.play('playerIdle');
        }
        if(this.animation.emitter && !this.animation.emitter.on) {
            this.animation.emitter.on = true;
        }
        if(this.isMe) {
            this.onPlayerStartFlight();
            this.gameObject.setVelocityY(-150);
        }
    }
    
    doStopFlight() {
        if(!this.isFlying && this.animation.emitter.on) {
            this.onPlayerStopFlight();
            this.animation.emitter.on = false;
        }
    }

    doIdle() {
        this.gameObject.play('playerIdle');
    }
}

class ControllablePlayer {
    playerObject = undefined;
    onDestroyTerrain = function() { };

    hitboxes = {
        left: undefined,
        right: undefined,
        bottom: undefined
    }

    keyMap = {
        'MOVE_LEFT' : [65], // a
        'MOVE_RIGHT' : [68], // d
        'MOVE_UP'   : [87], // w, space
        'MOVE_DOWN' : [83], // s
        'MOVE_FLY' : [32]
    }

    constructor(parent) {
        this.playerObject = parent;

        var playerContext = this.playerObject;
        var ctx = this;
        parent.phaser.input.keyboard.on('keydown', function(e) { ctx.bindKeyDown(playerContext, e.keyCode) });
        parent.phaser.input.keyboard.on('keyup', function(e) { ctx.bindKeyUp(playerContext, e.keyCode) });
    
        parent.phaser.cameras.main.startFollow(this.playerObject.gameObject, true);
        parent.phaser.cameras.main.setBounds(0, 0);

                
        this.hitboxes.bottom = parent.phaser.add.zone(0, 0).setSize(16,16);
        this.hitboxes.left = parent.phaser.add.zone(0, 0).setSize(16,16);
        this.hitboxes.right = parent.phaser.add.zone(0, 0).setSize(16,16);

        var ctx = this;
        Object.keys(this.hitboxes).forEach(function(hitbox) {
            if(!ctx.hitboxes[hitbox]) {
                return;
            }
            var hb = ctx.hitboxes[hitbox];
            parent.phaser.physics.world.enable(hb);
            hb.body.setAllowGravity(false);
            hb.body.moves = true;
        });
        if(parent.collidingGroups.length > 0) {
            parent.collidingGroups.forEach(function(group) {
                Object.keys(ctx.hitboxes).forEach(function(hitbox) {
                    if(!ctx.hitboxes[hitbox]) {
                        return;
                    }
                    parent.phaser.physics.add.overlap(ctx.hitboxes[hitbox], group, function(zone, image) {
                        var zoneDirection = hitbox;
                        ctx.onCollide(zoneDirection, image);
                    });
                });
            });
        }

    }

    onCollide(direction, terrainObject) {
        console.log('overlap detected', direction);
        var player = this.playerObject;
        if(player.isMovingDown && direction == 'bottom' && !parent.isFlying) {
            if(player.gameObject.anims.currentAnim.key !== 'playerMineVert') {
                player.gameObject.play('playerMineVert');
            }
            this.onDestroyTerrain(terrainObject.name);
        } if(player.isMovingLeft && direction == 'left' && !parent.isFlying) {
            if(player.gameObject.anims.currentAnim.key !== 'playerMineHor') {
                player.gameObject.play('playerMineHor');
                player.gameObject.setFlipX(true);
            }
            this.onDestroyTerrain(terrainObject.name);
        } if(player.isMovingRight && direction == 'right' && !parent.isFlying) {
            if(player.gameObject.anims.currentAnim.key !== 'playerMineHor') {
                player.gameObject.play('playerMineHor');
                player.gameObject.setFlipX(false);
            }
            this.onDestroyTerrain(terrainObject.name);
        }
    }

    bindKeyDown(playerContext, keyCode) {
        switch (keyCode) {
            case this.keyMap.MOVE_LEFT[0]:
                if(playerContext.isMovingDown) {
                    playerContext.isMovingDown = false;
                }
                playerContext.isMovingLeft = true;
                playerContext.isMovingRight = false;
                break;
            case this.keyMap.MOVE_RIGHT[0]:
                    if(playerContext.isMovingDown) {
                        playerContext.isMovingDown = false;
                    }
                playerContext.isMovingRight = true;
                playerContext.isMovingLeft = false;
                break;
            case this.keyMap.MOVE_UP[0]:
                playerContext.isMovingUp = true;
                break;
            case this.keyMap.MOVE_DOWN[0]: 
                if(!playerContext.isMovingRight && !playerContext.isMovingLeft) {
                    playerContext.isMovingDown = true;
                }
                break;
            case this.keyMap.MOVE_FLY[0]:
                playerContext.isFlying = true;
                break;
        }
    }
    bindKeyUp(playerContext, keyCode) {
        var playerContext = this.playerObject;
        switch (keyCode) {
            case this.keyMap.MOVE_LEFT[0]:
                playerContext.isMovingLeft = false;
                playerContext.doStopMoveLeftOrRight();
                break;
            case this.keyMap.MOVE_RIGHT[0]:
                playerContext.isMovingRight = false;
                playerContext.doStopMoveLeftOrRight();
                break;
            case this.keyMap.MOVE_DOWN[0]: 
                playerContext.isMovingDown = false;
                break;
            case this.keyMap.MOVE_UP[0]:
                playerContext.isMovingUp = false;
                break;
            case this.keyMap.MOVE_FLY[0]:
                playerContext.isFlying = false;
                playerContext.doStopFlight();
                break;
        }
    }
}