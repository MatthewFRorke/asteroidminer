class Sound {
    phaser;
    gameObject = {
        ambience: undefined,
        footStep1: undefined,
        footStep2: undefined,
    };
    config = {
        loop: true,
        volume: 0.5
    };
    footConfig = {
        loop: false,
        volume: 0.01
    };
    footstepTimeout = 20;
    footStepCount = 0;
    footStepType = 0;

    isBelowWorld = false;
    isFlying = false;
    
    constructor(phaserCtx) {
        this.phaser = phaserCtx;

        this.gameObject.ambience = this.phaser.sound.add('ambience', this.config);
        this.gameObject.ambience.play();

        this.gameObject.footStep1 = this.phaser.sound.add('footstep-1', this.footConfig);
        this.gameObject.footStep2 = this.phaser.sound.add('footstep-2', this.footConfig);

        this.gameObject.digDestroy = this.phaser.sound.add('dig-destroy', this.footConfig);
        
        this.gameObject.flight = this.phaser.sound.add('flight', { loop: true, volume: 0.025 });
    }

    setIsBelowWorld() {
        this.isBelowWorld = true;
    }

    setIsNotBelowWorld() {
        this.isBelowWorld = false;
    }

    checkAmbience() {
        if(this.isBelowWorld) {
            this.gameObject.ambience.volume = 0.1;
        } else if(this.gameObject.ambience.volume != 0.1) {
            this.gameObject.ambience.volume = this.config.volume;
        }
    }

    doFlight() {
        if(!this.isFlying) {
            this.gameObject.flight.play();
            this.isFlying = true;
        }
    }

    doStopFlight() {
        this.gameObject.flight.stop();
        this.isFlying = false;
    }

    doFootstep() {
        this.footStepCount++;
        if(this.footStepCount >= this.footstepTimeout) {
            if(this.footStepType == 0) {
                this.gameObject.footStep2.play();
                this.footStepType = 1;
            } else {
                this.gameObject.footStep1.play();
                this.footStepType = 0;
            }
            this.footStepCount = 0;
        }
    }

    doDigDestroy() {
        this.gameObject.digDestroy.play();
    }
}