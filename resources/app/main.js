const electron = require('electron');

// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

const usingNodemon = process && process.env ? process.env.USING_NODEMON == "TRUE" : false;


const path = require('path')
const url = require('url')
const ipcMain = electron.ipcMain;

const currentVersion = require('./version').version;

console.log('starting client', currentVersion);
const host = {
  url:"multiplayform.herokuapp.com",
  protocol:"https",
  port: 443
};
// const host = {
//   url:"localhost",
//   protocol:"http",
//   port: 8081
// };

ipcMain.on("update-required", (event, arg) => {
  var updateRequired = arg;
  
  if(updateRequired) {
    mainWindow.webContents.send('update-active-text', { text: "Updating" });
    doUpdate();
  } else {
    loadGame();
  }
})

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
function createWindow () {
  mainWindow = new BrowserWindow({
    width: 1024, height: 768, innerWidth: 1024, innerHeight: 768, autoHideMenuBar: true,
    webPreferences: {
      nodeIntegration: true,
    }
  })
  if(usingNodemon) {
    loadGame();
  } else {
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'Public/check-for-updates.html'),
      protocol: 'file:',
      slashes: true
    }));
    mainWindow.webContents.on('did-finish-load', () => { 
      mainWindow.webContents.send('version-checker', {version: String(currentVersion), ...host});
    })
  }
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

function doUpdate() {
  const http = require(host.protocol);
  const fs = require('fs');
  // mainWindow.webContents.send('update-active-text', { text: "Downloading Changes" });
  // res.pipe(file);
  const {shell} = require('electron');
  // Open a local file in the default app
  var shellI = shell.openItem(app.getAppPath() + '\\update\\update.bat');
  mainWindow.webContents.send('update-active-text', { text: "Downloading Changes" });
  app.exit(0);
}

function loadGame() {
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'Public/index.html'),
    protocol: 'file:',
    slashes: true
  }))
  mainWindow.webContents.on('did-finish-load', () => { 
    var includePort = host.port != 443 ? ":" + host.port : "";
    var fullRemoteHost = host.protocol + "://" + host.url + includePort;
    mainWindow.webContents.send('send-config', { version: currentVersion, remoteHost: fullRemoteHost, debug: usingNodemon });
  })
}