echo "Clearing any previous packages"
rd -r -fo out

echo "Packaging New Client"
electron-packager . --overwrite --platform=win32 --arch=ia32 --out=out --ignore=^/Server
cd out

echo "Moving Updater"
cp ../update ./test-win32-ia32/update -R
